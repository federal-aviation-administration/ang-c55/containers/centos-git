ARG REGISTRY_ADDRESS
ARG NAMESPACE
FROM $REGISTRY_ADDRESS/$NAMESPACE/centos-ssh:0.2

LABEL vendor="US DOT, FAA, Office of NextGen, Modeling and Simulation Branch" \
      version="0.2" \
      description="Docker container to support Git in CentOS"

RUN yum -y update && \
    yum clean all 

RUN yum -y groupinstall "Development Tools" && \
    yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel && \
    curl -sSL -o git.tar.gz https://github.com/git/git/archive/v2.20.1.tar.gz && \
    tar -zxf git.tar.gz && \
    cd git-* && \
    make configure && \
    ./configure --prefix=/usr/local && \
    make install && \ 
    yum -y groupremove "Development Tools" && \
    rm -rf /git-* && \
    rm -rf /git.tar.gz

